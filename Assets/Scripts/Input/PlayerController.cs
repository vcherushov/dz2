using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float _moveSpeed = 600;

    private PlayerInputActions _playerInputActions;

    private Rigidbody _rigidbody;
 

    // Start is called before the first frame update
    void Awake()
    {
        _playerInputActions = new PlayerInputActions();
        _rigidbody = GetComponent<Rigidbody>()
            
    }

    private void OnEnable()
    {
        _playerInputActions.Enable();
    }

    private void OnDisable()
    {
        _playerInputActions.Disable();
    }

    private void Update
    {
        if(_moveSpeed = 600f)
    {
        _moveSpeed = 1200f;
    }
        else{
        _moveSpeed = 600f;
        }
    }

    private void FixedUpdate()
    {

        Vector2 moveDirection = _playerInputActions.karting.Move.ReadValue<Vector2>();
        Move(moveDirection);

    }

    private void Move(Vector2 direction)
    {
        _rigidbody.velocity = new Vector3(direction.x * _moveSpeed * Time.deltaTime, 0f, direction.y * _moveSpeed * Time.deltaTime);
    }
    


}
